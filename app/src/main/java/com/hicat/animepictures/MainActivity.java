package com.hicat.animepictures;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.MatrixCursor;
import android.graphics.Color;
import android.media.AsyncPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.hicat.animepictures.R;
import com.hicat.animepictures.fragment.PageFragment;
import com.hicat.animepictures.model.AnimeDetail;
import com.hicat.animepictures.model.App;
import com.hicat.animepictures.model.Apps;
import com.hicat.animepictures.model.Search;
import com.hicat.animepictures.model.TagsList;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.lacostra.utils.notification.Rate;
import com.lacostra.utils.notification.json.Json;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.StringHolder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SwitchDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.rey.material.widget.ProgressView;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;
import net.rdrei.android.dirchooser.DirectoryChooserConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import badabing.lib.ServerUtilities;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("ResourceType")
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,SearchView.OnQueryTextListener
    ,SearchView.OnSuggestionListener
{

    private static JSONArray matrix;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    private LoadData loadData;
    private int index=0;
    private String tag_name;
    private String sort_by;
    private Boolean wall;
    private String how_recent;
    private String res_x;
    private String res_y;
    private String colo;
    private String user;
    private String favorite_by;
    private String folder;
    private String ext_jpg;
    private String ext_gif;
    private String ext_png;
    private SearchView serchView;
    private Sugestion suggestion;
    private String page;
    private Drawer drawerBuilder;
    private AdView adView;
    private Search search;
    private SharedPreferences appPreferences;
    private boolean isAppInstalled;

    @Override
    protected void onResume() {
        super.onResume();
        if(mSectionsPagerAdapter==null || mSectionsPagerAdapter.getCount()==0){
            ((ProgressView)findViewById(R.id.loading)).start();
            loadData= new LoadData();
            loadData.execute(getUriForPage(0));
        }
        Call<App> callApp= AnimeAplication.service.getApps(AnimeAplication.appsUrl);
        callApp.enqueue(new Callback<App>() {
            @Override
            public void onResponse(Call<App> call, Response<App> response) {
                if(response.isSuccessful()){
                    List<Apps> list= response.body().getTagsList();
                    for(Apps a: list){
                        if(a.getId()== (AnimeAplication.id)&& a.getStatus()==1)
                            MainActivity.this.finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<App> call, Throwable throwable) {

            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(loadData!=null && loadData.getStatus()== AsyncTask.Status.RUNNING)
            loadData.cancel(true);
    }

    public void gotoPage(int page){
        mViewPager.setCurrentItem(page);
    }
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(Color.parseColor("#ffcc00"));
        setSupportActionBar(toolbar);
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(3);

        drawerBuilder = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withHeader(R.layout.nav_header_main)
                .withTranslucentStatusBar(false)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.top_of_the_day).withIcon(GoogleMaterial.Icon.gmd_cloud_download).withIdentifier(1).withSelectable(false),
//                        new PrimaryDrawerItem().withName(R.string.Invite_friends).withIcon(GoogleMaterial.Icon.gmd_face).withIdentifier(2).withSelectable(false),
                        new PrimaryDrawerItem().withName(R.string.Filters).withIcon(GoogleMaterial.Icon.gmd_sort).withIdentifier(3).withSelectable(false),
                        new PrimaryDrawerItem().withName(R.string.Change_save_folder).withIcon(GoogleMaterial.Icon.gmd_folder).withIdentifier(5).withSelectable(false)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem.getIdentifier() == 3) {
                            startActivity(new Intent(MainActivity.this, SearchFilter.class));
                            drawerBuilder.closeDrawer();
                        }
                        if (drawerItem.getIdentifier() == 2) {
                            Intent intent = new AppInviteInvitation.IntentBuilder("Send App Invite").setMessage("A great app with tons of pictures!").setCallToActionText("Intall").build();
                            startActivityForResult(intent, 555);
                            drawerBuilder.closeDrawer();
                        }
                        if (drawerItem.getIdentifier() == 1) {
                            Intent intent = new Intent(MainActivity.this, MainActivity.class);
                            intent.putExtra("org.stalkerg.ap.search_sort_by", "rating");
                            intent.putExtra("org.stalkerg.ap.search_how_recent", "3");
                            startActivity(intent);
                            drawerBuilder.closeDrawer();
                        }
                        if (position == 4) {
                            AnimeAplication.showInterstitial();
                            final Intent chooserIntent = new Intent(MainActivity.this, DirectoryChooserActivity.class);

                            final DirectoryChooserConfig config = DirectoryChooserConfig.builder()
                                    .newDirectoryName(getString(R.string.app_name))
                                    .allowReadOnlyDirectory(true)
                                    .allowNewDirectoryNameModification(true)
                                    .build();
                            chooserIntent.putExtra(DirectoryChooserActivity.EXTRA_CONFIG, config);
                            // REQUEST_DIRECTORY is a constant integer to identify the request, e.g. 0
                            startActivityForResult(chooserIntent, 333);
                            drawerBuilder.closeDrawer();
                        }
                        return true;
                    }
                }).build();
        drawerBuilder.setSelection(1, false);
        fillExtras(getIntent());
        Rate.rate(this, getString(R.string.rate_message), getResources().getColor(R.color.colorAccent));
        this.adView = (AdView) this.findViewById(R.id.adView);
        AdRequest var13 = (new AdRequest.Builder()).build();
        this.adView.loadAd(var13);
        this.adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }
        });
        badabing.lib.handler.ExceptionHandler.register(
                this,  //contexto de la Activity
                "Error report Anime", //asunto del mensaje
                "qtjambiii@gmail.com");
        appPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        isAppInstalled = appPreferences.getBoolean("isAppInstalled", false);
        if (isAppInstalled == false) {
            /**
             * create short code
             */
            Intent shortcutIntent = new Intent(getApplicationContext(), MainActivity.class);
            shortcutIntent.setAction(Intent.ACTION_MAIN);
            Intent intent = new Intent();
            intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "ShortcutDemo");
            intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.mipmap.ic_launcher));
            intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(intent);
            /**
             * Make preference true
             */
            SharedPreferences.Editor editor = appPreferences.edit();
            editor.putBoolean("isAppInstalled", true);
            editor.commit();
        }
    }

  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==333 &&resultCode==DirectoryChooserActivity.RESULT_CODE_DIR_SELECTED){
            SharedPreferences pref= getSharedPreferences("app",MODE_PRIVATE);
            pref.edit().putString("store",data.getStringExtra(DirectoryChooserActivity.RESULT_SELECTED_DIR)).commit();
            AnimeAplication.store=pref.getString("store","");
            Toast.makeText(this,R.string.dir_set,Toast.LENGTH_SHORT).show();
        }
    }

    void fillExtras(Intent intent){

        tag_name = intent.getStringExtra("org.stalkerg.ap.tag_name");
        if(tag_name!=null){
        tag_name=tag_name.replace("<b>","");
        tag_name=tag_name.replace("</b>","");}
        sort_by = intent.getStringExtra("org.stalkerg.ap.search_sort_by");
        how_recent = intent.getStringExtra("org.stalkerg.ap.search_how_recent");
        res_x = intent.getStringExtra("org.stalkerg.ap.search_res_x");
        res_y = intent.getStringExtra("org.stalkerg.ap.search_res_y");
        colo = intent.getStringExtra("org.stalkerg.ap.search_color");
        user = intent.getStringExtra("org.stalkerg.ap.search_user");
        wall = Boolean.valueOf(intent.getBooleanExtra("org.stalkerg.ap.wallpapers_search", false));
        favorite_by = intent.getStringExtra("org.stalkerg.ap.search_favorite_by");
        folder = intent.getStringExtra("org.stalkerg.ap.search_favorite_folder");
        ext_jpg = intent.getStringExtra("org.stalkerg.ap.search_ext_jpg");
        ext_png = intent.getStringExtra("org.stalkerg.ap.search_ext_png");
        ext_gif = intent.getStringExtra("org.stalkerg.ap.search_ext_gif");
        page=intent.getStringExtra("page");
        if(page==null || TextUtils.isEmpty(page))
            page="0";
    }

   public String getUriForPage(int pageNumbre){
        android.net.Uri.Builder builder = new android.net.Uri.Builder();
        builder.scheme("https").authority("anime-pictures.net").appendPath("pictures").appendPath("view_posts");
        builder.appendPath(Integer.toString(pageNumbre)).appendQueryParameter("type", "json").appendQueryParameter("lang", "en").appendQueryParameter("posts_per_page", Integer.toString(10));
        if(sort_by!=null)
            builder.appendQueryParameter("order_by",sort_by);
        if(how_recent!=null)
            builder.appendQueryParameter("ldate",how_recent);
        if(res_x!=null)
            builder.appendQueryParameter("res_x",res_x);
        if(res_y!=null)
            builder.appendQueryParameter("res_y",res_y);
        if(colo!=null)
            builder.appendQueryParameter("color",colo);
        if(user!=null)
            builder.appendQueryParameter("user",user);
        if(ext_jpg!=null)
            builder.appendQueryParameter("ext_jpg", ext_jpg);
       if(ext_png!=null)
           builder.appendQueryParameter("ext_png",ext_png);
       if(ext_gif!=null)
           builder.appendQueryParameter("ext_gif",ext_gif);
       if(tag_name!=null)
           builder.appendQueryParameter("search_tag",tag_name);
//        if (p.length() != 0)
//        {
//            ((android.net.Uri.Builder) (obj)).appendQueryParameter("search_tag", p);
//        }
//
//        if (x != null && a)
//        {
//            ((android.net.Uri.Builder) (obj)).appendQueryParameter("favorite_by", x);
//        }
//        if (y != null && a)
//        {
//            ((android.net.Uri.Builder) (obj)).appendQueryParameter("favorite_folder", y);
//        }
        Log.d("URL",builder.toString());
        return builder.toString();
    }


    String [] columns= new String[]{"_id","t"};
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_search, menu);
        serchView=(SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        serchView.setOnSuggestionListener(this);
        serchView.setOnQueryTextListener(this);
        suggestion=new Sugestion(this,R.layout.material_drawer_item_edit,null,columns,null,-1000);
        serchView.setSuggestionsAdapter(suggestion);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (query.length() > 2) {
            new LoadTags().execute(new String[]{query});
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() > 2) {
            new LoadTags().execute(new String[]{newText});
        }
        return true;
    }

    @Override
    public boolean onSuggestionSelect(int position) {
     return true;
    }

    @Override
    public boolean onSuggestionClick(int position) {
        Intent intent= new Intent(this,MainActivity.class);
        try {
            intent.putExtra("org.stalkerg.ap.tag_name",search.getTagsList().get(position).getT());
            InputMethodManager obj = (InputMethodManager)getSystemService("input_method");
            if (obj != null)
            {
                ((InputMethodManager) (obj)).hideSoftInputFromWindow(serchView.getWindowToken(), 0);
            }
            serchView.clearFocus();
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Fragment fragment=PageFragment.newInstance(position+"",AnimeAplication.total+"");
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return AnimeAplication.total;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }

    class LoadData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPostExecute(String aVoid) {
            if(aVoid==null)
                return;
            try {
                ((ProgressView)findViewById(R.id.loading)).stop();
                JSONObject object=new JSONObject(aVoid);
                AnimeAplication.total=object.getInt("max_pages");
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
                // Set up the ViewPager with the sections adapter.

                mViewPager.setAdapter(mSectionsPagerAdapter);
                mViewPager.setOffscreenPageLimit(1);
//                getSharedPreferences("pref",MODE_PRIVATE).edit().putInt(Constants.POST_COUNT,AnimeAplication.data.getInt(""))
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String data=Json.getJSONString(params[0]);
//            data=Json.getJSONString(Constants.listUrl);
            if(isCancelled())
                return null;
            return data;

        }

    }

    public class LoadTags extends AsyncTask<String,Void,Void>{

        @Override
        protected Void doInBackground(String... params) {
//            String result = Json.getJSONString("http://anime-pictures.net/pictures/autocomplete_tag?tag="+params[0]);
//            String result = Json.getJSONString("http://192.168.137.1/anime/search");
//            try {
//                return new JSONObject(result).getJSONArray("tags_list");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return null;

            Call<Search> searchCall= AnimeAplication.service.getTags("http://anime-pictures.net/pictures/autocomplete_tag?tag="+params[0]);
            try {
                search= searchCall.execute().body();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void jsonArray) {
            MatrixCursor matrix= new MatrixCursor(columns);
            if(search!=null){

            for (int i = 0; i < search.getTagsList().size(); i++) {
                TagsList tag= search.getTagsList().get(i);
                try {
                    String text= tag.getT().replace("<b>","");
                    text= text.replace("</b>","");
                    matrix.addRow(new String[]{tag.getId()+"",text});
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            suggestion.changeCursor(matrix);
            }
          }
    }
}