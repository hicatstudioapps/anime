package com.hicat.animepictures;

import com.hicat.animepictures.model.AnimeDetail;
import com.hicat.animepictures.model.App;
import com.hicat.animepictures.model.Apps;
import com.hicat.animepictures.model.ExampleAnime;
import com.hicat.animepictures.model.Search;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by CQ on 24/04/2016.
 */
public interface IService {

    @GET
    public Call<ExampleAnime> getPost(@Url String url);
    @GET
    public
    Call<AnimeDetail> getDetail(@Url String url);

    @POST
    public Call<Search> getTags(@Url String url);

    @GET
    public Call<App> getApps(@Url String url);

}
