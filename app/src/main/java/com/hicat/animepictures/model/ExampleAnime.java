
package com.hicat.animepictures.model;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class ExampleAnime {

    @SerializedName("max_pages")
    @Expose
    private Integer maxPages;
    @SerializedName("response_posts_count")
    @Expose
    private Integer responsePostsCount;
    @SerializedName("posts_per_page")
    @Expose
    private Integer postsPerPage;
    @SerializedName("posts")
    @Expose
    private List<Post> posts = new ArrayList<Post>();
    @SerializedName("page_number")
    @Expose
    private Integer pageNumber;
    @SerializedName("posts_count")
    @Expose
    private Integer postsCount;

    /**
     * 
     * @return
     *     The maxPages
     */
    public Integer getMaxPages() {
        return maxPages;
    }

    /**
     * 
     * @param maxPages
     *     The max_pages
     */
    public void setMaxPages(Integer maxPages) {
        this.maxPages = maxPages;
    }

    /**
     * 
     * @return
     *     The responsePostsCount
     */
    public Integer getResponsePostsCount() {
        return responsePostsCount;
    }

    /**
     * 
     * @param responsePostsCount
     *     The response_posts_count
     */
    public void setResponsePostsCount(Integer responsePostsCount) {
        this.responsePostsCount = responsePostsCount;
    }

    /**
     * 
     * @return
     *     The postsPerPage
     */
    public Integer getPostsPerPage() {
        return postsPerPage;
    }

    /**
     * 
     * @param postsPerPage
     *     The posts_per_page
     */
    public void setPostsPerPage(Integer postsPerPage) {
        this.postsPerPage = postsPerPage;
    }

    /**
     * 
     * @return
     *     The posts
     */
    public List<Post> getPosts() {
        return posts;
    }

    /**
     * 
     * @param posts
     *     The posts
     */
    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    /**
     * 
     * @return
     *     The pageNumber
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * 
     * @param pageNumber
     *     The page_number
     */
    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    /**
     * 
     * @return
     *     The postsCount
     */
    public Integer getPostsCount() {
        return postsCount;
    }

    /**
     * 
     * @param postsCount
     *     The posts_count
     */
    public void setPostsCount(Integer postsCount) {
        this.postsCount = postsCount;
    }

}
