
package com.hicat.animepictures.model;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class TagsList {

    @SerializedName("c")
    @Expose
    private Integer c;
    @SerializedName("t2")
    @Expose
    private Object t2;
    @SerializedName("t")
    @Expose
    private String t;
    @SerializedName("id")
    @Expose
    private Integer id;

    /**
     * 
     * @return
     *     The c
     */
    public Integer getC() {
        return c;
    }

    /**
     * 
     * @param c
     *     The c
     */
    public void setC(Integer c) {
        this.c = c;
    }

    /**
     * 
     * @return
     *     The t2
     */
    public Object getT2() {
        return t2;
    }

    /**
     * 
     * @param t2
     *     The t2
     */
    public void setT2(Object t2) {
        this.t2 = t2;
    }

    /**
     * 
     * @return
     *     The t
     */
    public String getT() {
        return t;
    }

    /**
     * 
     * @param t
     *     The t
     */
    public void setT(String t) {
        this.t = t;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

}
