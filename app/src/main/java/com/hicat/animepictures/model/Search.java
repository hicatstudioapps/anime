
package com.hicat.animepictures.model;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Search {

    @SerializedName("tags_list")
    @Expose
    private List<TagsList> tagsList = new ArrayList<TagsList>();

    /**
     * 
     * @return
     *     The tagsList
     */
    public List<TagsList> getTagsList() {
        return tagsList;
    }

    /**
     * 
     * @param tagsList
     *     The tags_list
     */
    public void setTagsList(List<TagsList> tagsList) {
        this.tagsList = tagsList;
    }

}
