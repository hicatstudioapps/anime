
package com.hicat.animepictures.model;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Post {

    @SerializedName("medium_preview")
    @Expose
    private String mediumPreview;
    @SerializedName("erotics")
    @Expose
    private Integer erotics;
    @SerializedName("small_preview")
    @Expose
    private String smallPreview;
    @SerializedName("big_preview")
    @Expose
    private String bigPreview;
    @SerializedName("ext")
    @Expose
    private String ext;
    @SerializedName("score_number")
    @Expose
    private Integer scoreNumber;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("md5_pixels")
    @Expose
    private String md5Pixels;
    @SerializedName("score")
    @Expose
    private Integer score;
    @SerializedName("pubtime")
    @Expose
    private String pubtime;
    @SerializedName("download_count")
    @Expose
    private Integer downloadCount;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("color")
    @Expose
    private List<Integer> color = new ArrayList<Integer>();
    @SerializedName("md5")
    @Expose
    private String md5;

    /**
     * 
     * @return
     *     The mediumPreview
     */
    public String getMediumPreview() {
        return mediumPreview;
    }

    /**
     * 
     * @param mediumPreview
     *     The medium_preview
     */
    public void setMediumPreview(String mediumPreview) {
        this.mediumPreview = mediumPreview;
    }

    /**
     * 
     * @return
     *     The erotics
     */
    public Integer getErotics() {
        return erotics;
    }

    /**
     * 
     * @param erotics
     *     The erotics
     */
    public void setErotics(Integer erotics) {
        this.erotics = erotics;
    }

    /**
     * 
     * @return
     *     The smallPreview
     */
    public String getSmallPreview() {
        return smallPreview;
    }

    /**
     * 
     * @param smallPreview
     *     The small_preview
     */
    public void setSmallPreview(String smallPreview) {
        this.smallPreview = smallPreview;
    }

    /**
     * 
     * @return
     *     The bigPreview
     */
    public String getBigPreview() {
        return bigPreview;
    }

    /**
     * 
     * @param bigPreview
     *     The big_preview
     */
    public void setBigPreview(String bigPreview) {
        this.bigPreview = bigPreview;
    }

    /**
     * 
     * @return
     *     The ext
     */
    public String getExt() {
        return ext;
    }

    /**
     * 
     * @param ext
     *     The ext
     */
    public void setExt(String ext) {
        this.ext = ext;
    }

    /**
     * 
     * @return
     *     The scoreNumber
     */
    public Integer getScoreNumber() {
        return scoreNumber;
    }

    /**
     * 
     * @param scoreNumber
     *     The score_number
     */
    public void setScoreNumber(Integer scoreNumber) {
        this.scoreNumber = scoreNumber;
    }

    /**
     * 
     * @return
     *     The height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * 
     * @param height
     *     The height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * 
     * @return
     *     The width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * 
     * @param width
     *     The width
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * 
     * @return
     *     The md5Pixels
     */
    public String getMd5Pixels() {
        return md5Pixels;
    }

    /**
     * 
     * @param md5Pixels
     *     The md5_pixels
     */
    public void setMd5Pixels(String md5Pixels) {
        this.md5Pixels = md5Pixels;
    }

    /**
     * 
     * @return
     *     The score
     */
    public Integer getScore() {
        return score;
    }

    /**
     * 
     * @param score
     *     The score
     */
    public void setScore(Integer score) {
        this.score = score;
    }

    /**
     * 
     * @return
     *     The pubtime
     */
    public String getPubtime() {
        return pubtime;
    }

    /**
     * 
     * @param pubtime
     *     The pubtime
     */
    public void setPubtime(String pubtime) {
        this.pubtime = pubtime;
    }

    /**
     * 
     * @return
     *     The downloadCount
     */
    public Integer getDownloadCount() {
        return downloadCount;
    }

    /**
     * 
     * @param downloadCount
     *     The download_count
     */
    public void setDownloadCount(Integer downloadCount) {
        this.downloadCount = downloadCount;
    }

    /**
     * 
     * @return
     *     The size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * 
     * @param size
     *     The size
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The color
     */
    public List<Integer> getColor() {
        return color;
    }

    /**
     * 
     * @param color
     *     The color
     */
    public void setColor(List<Integer> color) {
        this.color = color;
    }

    /**
     * 
     * @return
     *     The md5
     */
    public String getMd5() {
        return md5;
    }

    /**
     * 
     * @param md5
     *     The md5
     */
    public void setMd5(String md5) {
        this.md5 = md5;
    }

}
