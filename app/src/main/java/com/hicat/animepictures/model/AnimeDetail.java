
package com.hicat.animepictures.model;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class AnimeDetail {

    @SerializedName("color")
    @Expose
    private List<Integer> color = new ArrayList<Integer>();
    @SerializedName("favorite_folder")
    @Expose
    private String favoriteFolder;
    @SerializedName("big_preview")
    @Expose
    private String bigPreview;
    @SerializedName("score_number")
    @Expose
    private Integer scoreNumber;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("md5_pixels")
    @Expose
    private String md5Pixels;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("medium_preview")
    @Expose
    private String mediumPreview;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("small_preview")
    @Expose
    private String smallPreview;
    @SerializedName("star_it")
    @Expose
    private Boolean starIt;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("score")
    @Expose
    private Integer score;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("user_avatar")
    @Expose
    private String userAvatar;
    @SerializedName("tags")
    @Expose
    private List<String> tags = new ArrayList<String>();
    @SerializedName("erotics")
    @Expose
    private Integer erotics;
    @SerializedName("tags_full")
    @Expose
    private List<TagsFull> tagsFull = new ArrayList<TagsFull>();
    @SerializedName("download_count")
    @Expose
    private Integer downloadCount;
    @SerializedName("user_favorite_folders")
    @Expose
    private Object userFavoriteFolders;
    @SerializedName("md5")
    @Expose
    private String md5;
    @SerializedName("is_favorites")
    @Expose
    private Boolean isFavorites;
    @SerializedName("file_url")
    @Expose
    private String fileUrl;
    @SerializedName("ext")
    @Expose
    private String ext;
    @SerializedName("pubtime")
    @Expose
    private String pubtime;

    /**
     * 
     * @return
     *     The color
     */
    public List<Integer> getColor() {
        return color;
    }

    /**
     * 
     * @param color
     *     The color
     */
    public void setColor(List<Integer> color) {
        this.color = color;
    }

    /**
     * 
     * @return
     *     The favoriteFolder
     */
    public String getFavoriteFolder() {
        return favoriteFolder;
    }

    /**
     * 
     * @param favoriteFolder
     *     The favorite_folder
     */
    public void setFavoriteFolder(String favoriteFolder) {
        this.favoriteFolder = favoriteFolder;
    }

    /**
     * 
     * @return
     *     The bigPreview
     */
    public String getBigPreview() {
        return bigPreview;
    }

    /**
     * 
     * @param bigPreview
     *     The big_preview
     */
    public void setBigPreview(String bigPreview) {
        this.bigPreview = bigPreview;
    }

    /**
     * 
     * @return
     *     The scoreNumber
     */
    public Integer getScoreNumber() {
        return scoreNumber;
    }

    /**
     * 
     * @param scoreNumber
     *     The score_number
     */
    public void setScoreNumber(Integer scoreNumber) {
        this.scoreNumber = scoreNumber;
    }

    /**
     * 
     * @return
     *     The height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * 
     * @param height
     *     The height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * 
     * @return
     *     The md5Pixels
     */
    public String getMd5Pixels() {
        return md5Pixels;
    }

    /**
     * 
     * @param md5Pixels
     *     The md5_pixels
     */
    public void setMd5Pixels(String md5Pixels) {
        this.md5Pixels = md5Pixels;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * 
     * @param size
     *     The size
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * 
     * @return
     *     The mediumPreview
     */
    public String getMediumPreview() {
        return mediumPreview;
    }

    /**
     * 
     * @param mediumPreview
     *     The medium_preview
     */
    public void setMediumPreview(String mediumPreview) {
        this.mediumPreview = mediumPreview;
    }

    /**
     * 
     * @return
     *     The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     The smallPreview
     */
    public String getSmallPreview() {
        return smallPreview;
    }

    /**
     * 
     * @param smallPreview
     *     The small_preview
     */
    public void setSmallPreview(String smallPreview) {
        this.smallPreview = smallPreview;
    }

    /**
     * 
     * @return
     *     The starIt
     */
    public Boolean getStarIt() {
        return starIt;
    }

    /**
     * 
     * @param starIt
     *     The star_it
     */
    public void setStarIt(Boolean starIt) {
        this.starIt = starIt;
    }

    /**
     * 
     * @return
     *     The width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * 
     * @param width
     *     The width
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * 
     * @return
     *     The score
     */
    public Integer getScore() {
        return score;
    }

    /**
     * 
     * @param score
     *     The score
     */
    public void setScore(Integer score) {
        this.score = score;
    }

    /**
     * 
     * @return
     *     The userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 
     * @param userName
     *     The user_name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 
     * @return
     *     The userAvatar
     */
    public String getUserAvatar() {
        return userAvatar;
    }

    /**
     * 
     * @param userAvatar
     *     The user_avatar
     */
    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    /**
     * 
     * @return
     *     The tags
     */
    public List<String> getTags() {
        return tags;
    }

    /**
     * 
     * @param tags
     *     The tags
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     * 
     * @return
     *     The erotics
     */
    public Integer getErotics() {
        return erotics;
    }

    /**
     * 
     * @param erotics
     *     The erotics
     */
    public void setErotics(Integer erotics) {
        this.erotics = erotics;
    }

    /**
     * 
     * @return
     *     The tagsFull
     */
    public List<TagsFull> getTagsFull() {
        return tagsFull;
    }

    /**
     * 
     * @param tagsFull
     *     The tags_full
     */
    public void setTagsFull(List<TagsFull> tagsFull) {
        this.tagsFull = tagsFull;
    }

    /**
     * 
     * @return
     *     The downloadCount
     */
    public Integer getDownloadCount() {
        return downloadCount;
    }

    /**
     * 
     * @param downloadCount
     *     The download_count
     */
    public void setDownloadCount(Integer downloadCount) {
        this.downloadCount = downloadCount;
    }

    /**
     * 
     * @return
     *     The userFavoriteFolders
     */
    public Object getUserFavoriteFolders() {
        return userFavoriteFolders;
    }

    /**
     * 
     * @param userFavoriteFolders
     *     The user_favorite_folders
     */
    public void setUserFavoriteFolders(Object userFavoriteFolders) {
        this.userFavoriteFolders = userFavoriteFolders;
    }

    /**
     * 
     * @return
     *     The md5
     */
    public String getMd5() {
        return md5;
    }

    /**
     * 
     * @param md5
     *     The md5
     */
    public void setMd5(String md5) {
        this.md5 = md5;
    }

    /**
     * 
     * @return
     *     The isFavorites
     */
    public Boolean getIsFavorites() {
        return isFavorites;
    }

    /**
     * 
     * @param isFavorites
     *     The is_favorites
     */
    public void setIsFavorites(Boolean isFavorites) {
        this.isFavorites = isFavorites;
    }

    /**
     * 
     * @return
     *     The fileUrl
     */
    public String getFileUrl() {
        return fileUrl;
    }

    /**
     * 
     * @param fileUrl
     *     The file_url
     */
    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    /**
     * 
     * @return
     *     The ext
     */
    public String getExt() {
        return ext;
    }

    /**
     * 
     * @param ext
     *     The ext
     */
    public void setExt(String ext) {
        this.ext = ext;
    }

    /**
     * 
     * @return
     *     The pubtime
     */
    public String getPubtime() {
        return pubtime;
    }

    /**
     * 
     * @param pubtime
     *     The pubtime
     */
    public void setPubtime(String pubtime) {
        this.pubtime = pubtime;
    }

}
