package com.hicat.animepictures;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.hicat.animepictures.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SearchFilter extends AppCompatActivity {

    @Bind(R.id.resolution_width)
    EditText res_x;
    @Bind(R.id.resolution_height)
    EditText res_y;
    @Bind(R.id.sorted_by_group)
    RadioGroup group_sort;
    @Bind(R.id.how_recent_group)
    RadioGroup group_recnt;
    @Bind(R.id.ext_jpg)
    CheckBox jpg;
    @Bind(R.id.ext_png)
    CheckBox png;
    @Bind(R.id.ext_gif)
    CheckBox gif;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filters_menu,menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_filter);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        AnimeAplication.showInterstitial();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filters_action_done:
                Intent intent = new Intent(this, MainActivity.class);
                //resolucion
                if (!TextUtils.isEmpty(res_x.getText())) {
                    intent.putExtra("org.stalkerg.ap.search_res_x", res_x.getText().toString());
                }
                if (!TextUtils.isEmpty(res_y.getText()))
                    intent.putExtra("org.stalkerg.ap.search_res_y", res_y.getText().toString());
                switch (group_sort.getCheckedRadioButtonId()) {
                    case R.id.sort_by_date:
                        intent.putExtra("org.stalkerg.ap.search_sort_by", "date");
                        break;
                    case R.id.sort_by_date_revers:
                        intent.putExtra("org.stalkerg.ap.search_sort_by", "date_r");
                        break;
                    case R.id.sort_by_rating:
                        intent.putExtra("org.stalkerg.ap.search_sort_by", "rating");

                    case R.id.sort_by_downloads:
                        intent.putExtra("org.stalkerg.ap.search_sort_by", "views");
                        break;
                    case R.id.sort_by_size:
                        intent.putExtra("org.stalkerg.ap.search_sort_by", "size");
                        break;
                }
                switch (group_recnt.getCheckedRadioButtonId()) {
                    case R.id.recent_anytime:
                        intent.putExtra("org.stalkerg.ap.search_how_recent", "0");
                        break;
                    case R.id.recent_past_day:
                        intent.putExtra("org.stalkerg.ap.search_how_recent", "3");
                        break;
                    case R.id.recent_past_week:
                        intent.putExtra("org.stalkerg.ap.search_how_recent", "1");
                        break;
                    case R.id.recent_past_month:
                        intent.putExtra("org.stalkerg.ap.search_how_recent", "2");
                        break;
                }
                if (jpg.isChecked())
                    intent.putExtra("org.stalkerg.ap.search_ext_jpg", "jpg");
                if (png.isChecked())
                    intent.putExtra("org.stalkerg.ap.search_ext_png", "png");
                if (gif.isChecked())
                    intent.putExtra("org.stalkerg.ap.search_ext_gif", "gif");
                startActivity(intent);
                finish();
            break;
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
