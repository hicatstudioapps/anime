package com.hicat.animepictures.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hicat.animepictures.R;
import com.hicat.animepictures.AnimeAplication;
import com.hicat.animepictures.Constants;
import com.hicat.animepictures.GridSpacingItemDecoration;
import com.hicat.animepictures.MainActivity;
import com.hicat.animepictures.adapter.PageRV;
import com.hicat.animepictures.model.ExampleAnime;
import com.lacostra.utils.notification.json.Json;
import com.rey.material.widget.ProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PageFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PageFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String fragNumber;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    @Bind(R.id.recycler)
    RecyclerView recyclerView;
    private Context context;
    @Bind(R.id.loading)
    ProgressView progressView;
    @Bind(R.id.current)
    EditText current;
    @Bind(R.id.total)
    TextView total;
    private LoadData loadData;

    public PageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PageFragment newInstance(String start, String param2) {
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, start);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fragNumber = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root= inflater.inflate(R.layout.fragment_page, container, false);;
        ButterKnife.bind(this, root);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
//        recyclerView.setOrientation(TwoWayLayoutManager.Orientation.VERTICAL);
//        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, 20, false));
        progressView.start();
        current.setText(Integer.parseInt(fragNumber) + 1 + "");
        current.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==66){
                    ((MainActivity)context).gotoPage(Integer.parseInt(current.getText().toString())-1);
                }
                return false;
            }
        });
        total.setText(mParam2);
        loadData= new LoadData();
        String url= ((MainActivity)context).getUriForPage(Integer.parseInt(fragNumber));
        Call<ExampleAnime> call= AnimeAplication.service.getPost(url);
//         call= AnimeAplication.service.getPost("http://192.168.101.1:8081/anime/jsonAll.txt");
        call.enqueue(new Callback<ExampleAnime>() {
            @Override
            public void onResponse(Call<ExampleAnime> call, Response<ExampleAnime> response) {
                if(response.isSuccessful()){
                    result=response.body();
                    recyclerView.setAdapter(new PageRV(context,result.getPosts()));
                    progressView.stop();
                }
            }

            @Override
            public void onFailure(Call<ExampleAnime> call, Throwable t) {
                Log.d("page error",t.toString());
            }
        });
        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       this.context=context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (loadData != null && loadData.getStatus() == AsyncTask.Status.RUNNING)
            loadData.cancel(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    ExampleAnime result;

    class LoadData extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid) {
            if(result==null)
                return;

                recyclerView.setAdapter(new PageRV(context,result.getPosts()));
                progressView.stop();
//                getSharedPreferences("pref",MODE_PRIVATE).edit().putInt(Constants.POST_COUNT,AnimeAplication.data.getInt(""))

        }

        @Override
        protected Void doInBackground(String... params) {
         //   String data=Json.getJSONString(params[0]);
//            Call<ExampleAnime> call= AnimeAplication.service.getPost(params[0]);
            Call<ExampleAnime> call= AnimeAplication.service.getPost("http://192.168.101.1:8081/anime/jsonAll.txt");
            call.enqueue(new Callback<ExampleAnime>() {
                @Override
                public void onResponse(Call<ExampleAnime> call, Response<ExampleAnime> response) {
                    if(response.isSuccessful()){
                        result=response.body();
                    }
                }

                @Override
                public void onFailure(Call<ExampleAnime> call, Throwable t) {
                    Log.d("page error",t.toString());
                }
            });
            if(isCancelled())
                return null;
           return null;
        }

    }
}
