package com.hicat.animepictures;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.hicat.animepictures.R;

/**
 * Created by CQ on 14/03/2016.
 */
public class Sugestion extends android.support.v4.widget.SimpleCursorAdapter {

    private Context context;

    public Sugestion(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        this.context=context;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ((TextView)view.findViewById(R.id.nv_total)).setText(cursor.getString(1));
    }
}
