package com.hicat.animepictures;

import android.app.WallpaperManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hicat.animepictures.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hicat.animepictures.model.AnimeDetail;
import com.hicat.animepictures.model.ExampleAnime;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.lacostra.utils.notification.Download.DowloadManager;
import com.lacostra.utils.notification.Permission.Permission;
import com.lacostra.utils.notification.Permission.PermissionActivity;
import com.lacostra.utils.notification.json.Json;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.rey.material.widget.ProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//import jp.wasabeef.blurry.Blurry;

@SuppressWarnings("ResourceType")
public class PictureActivity extends PermissionActivity {

    private static boolean enableOptions=false;
    @Bind(R.id.picture_extension)
    TextView extension;
    @Bind(R.id.picture_resolution)
    TextView resolution;
    @Bind(R.id.picture_size_new)
    TextView size;
    @Bind(R.id.picture_votes_num)
    TextView votes;
    @Bind(R.id.picture_big_image)
    ImageView imageView;
    @Bind(R.id.picture_title)
    TextView user;
    @Bind(R.id.avatar_image)
    ImageView avatar;
    @Bind(R.id.picture_big_layout)
    android.widget.RelativeLayout blur_target;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    String fileURL;
    String fileDestination;
    @Bind(R.id.loading)
    ProgressView progress;
    private LoadData loadData;
    private AdView adView;
    private AnimeDetail result;
    private Uri fileTempURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);
        ButterKnife.bind(this);
        toolbar.setTitle("No " + AnimeAplication.detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        this.adView = (AdView)this.findViewById(R.id.adView);
        AdRequest var13 = (new AdRequest.Builder()).build();
        this.adView.loadAd(var13);
        this.adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }
        });

        badabing.lib.handler.ExceptionHandler.register(
                this,  //contexto de la Activity
                "Error report Anime", //asunto del mensaje
                "qtjambiii@gmail.com");
//        String data= Json.getJSONString("http://anime-pictures.net/pictures/view_post/"+AnimeAplication.detail+"?type=json&lang=en");
        Call<AnimeDetail> call= AnimeAplication.service.getDetail("http://anime-pictures.net/pictures/view_post/"+AnimeAplication.detail+"?type=json&lang=en");
//        call= AnimeAplication.service.getDetail("http://192.168.101.1:8081/anime/post");
        call.enqueue(new Callback<AnimeDetail>() {
            @Override
            public void onResponse(Call<AnimeDetail> call, Response<AnimeDetail> response) {
                if (response.isSuccessful()) {
                    result = response.body();
                    progress.stop();
                    if(result!=null) {
                        Glide.with(PictureActivity.this).load(result.getUserAvatar()).diskCacheStrategy(DiskCacheStrategy.RESULT).into(avatar);
                        user.setText(result.getUserName());
                    }

                }
            }

            @Override
            public void onFailure(Call<AnimeDetail> call, Throwable t) {
                Log.d("fallo",AnimeAplication.detail+"");
            }
        });
        ImageLoader.getInstance().displayImage(AnimeAplication.medium_preview, imageView, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                File temp= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                temp= new File(temp,"temp.png");
                try {
                    FileOutputStream fileOutputStream= new FileOutputStream(temp);
                    bitmap.compress(Bitmap.CompressFormat.PNG,100,fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    fileTempURI= Uri.parse("file://"+temp.getAbsolutePath());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                PictureActivity.enableOptions = true;
                AnimeAplication.showInterstitial();
                progress.stop();
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
        size.setText(Integer.parseInt(AnimeAplication.size)/1024 + " Kb");
        resolution.setText(AnimeAplication.height+"x"+AnimeAplication.width);
        extension.setText(AnimeAplication.ext);
        votes.setText(AnimeAplication.score_number);

        List<Integer> color= AnimeAplication.color;
        int bgColor= Color.rgb(AnimeAplication.color.get(0), AnimeAplication.color.get(1), AnimeAplication.color.get(2));
        ((ImageView)findViewById(R.id.imageView4)).setBackgroundColor(bgColor);
        fileURL=AnimeAplication.big_preview;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode==1){
            if(Permission.hasPermission(PictureActivity.this,Permission.WRITE_EXTERNAL)){
//                DonwLoad();
            }
            else
                Permission.fail(PictureActivity.this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
            case R.id.picture_action_download:
                if (enableOptions) {
                    if (Permission.hasPermission(PictureActivity.this, Permission.WRITE_EXTERNAL)) {
                        DonwLoad();
                    } else {
                        ArrayList list = new ArrayList(1);
                        list.add(Permission.WRITE_EXTERNAL);
                        askPermision(list, 1);
                    }
                }
                break;
            case R.id.picture_action_set:
                if (Permission.hasPermission(PictureActivity.this, Permission.WRITE_EXTERNAL)) {
                    if (enableOptions) {
                        File saveDir = new File(AnimeAplication.store);
                        File[] files = saveDir.listFiles();
                        if (files.length == 0) {
                            Toast.makeText(this, R.string.wallpaper_no, Toast.LENGTH_LONG).show();
                            if (Permission.hasPermission(PictureActivity.this, Permission.WRITE_EXTERNAL)) {
                                DonwLoad();
                            } else {
                                ArrayList list = new ArrayList(1);
                                list.add(Permission.WRITE_EXTERNAL);
                                askPermision(list, 1);
                            }
                            break;
                        }
                        boolean succes = false;
                        for (int i = 0; i < files.length; i++) {
                            String name = files[i].getName();
                            if (name.contains(AnimeAplication.detail + "")) {
                                WallpaperManager m = WallpaperManager.getInstance(this);
                                try {
                                    m.setStream(new FileInputStream(files[i].getAbsolutePath()));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(this, R.string.wallpaper_ok, Toast.LENGTH_SHORT).show();
                                succes = true;
                                break;
                            }

                        }
                        if (!succes) {
                            DonwLoad();
                            Toast.makeText(this, R.string.wallpaper_no, Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    ArrayList list = new ArrayList(1);
                    list.add(Permission.WRITE_EXTERNAL);
                    askPermision(list, 1);
                }


                break;
            case R.id.picture_action_copy:
                if (enableOptions) {
                    ClipboardManager clipboardmanager = (ClipboardManager) getSystemService("clipboard");
                    Uri uri = Uri.parse(fileURL);
                    clipboardmanager.setPrimaryClip(ClipData.newUri(getContentResolver(), "URI", uri));
                    Toast.makeText(this, R.string.clipBoard, Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.picture_action_share:
                if (Permission.hasPermission(PictureActivity.this, Permission.WRITE_EXTERNAL)) {
                    Intent share_picture= new Intent(Intent.ACTION_SEND);
                    share_picture.setType("image/png")
                            .putExtra(Intent.EXTRA_STREAM,fileTempURI)
                    .putExtra(Intent.EXTRA_TEXT,AnimeAplication.big_preview);
                    startActivity(share_picture);
                }else {
                    ArrayList list = new ArrayList(1);
                    list.add(Permission.WRITE_EXTERNAL);
                    askPermision(list, 1);
                }
                break;
        }return true;
    }

    private void DonwLoad() {

        File saveDir= new File(AnimeAplication.store);
        if(!saveDir.exists())
            saveDir.mkdir();
        String extention= fileURL.substring(fileURL.length()-4,fileURL.length());
        String uri=(saveDir.getAbsoluteFile() + "/" + AnimeAplication.detail + extention);
        DowloadManager manager= new DowloadManager(this);
        manager.download(Uri.parse(fileURL),getApplicationInfo().name,"", Uri.fromFile(new File(uri)));
        fileDestination=uri;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.picture_menu,menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(loadData!=null && loadData.getStatus()== AsyncTask.Status.RUNNING)
                loadData.cancel(true);

    }

    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {
        if (menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);

                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return super.onPrepareOptionsPanel(view, menu);
    }
    @Override
    protected void onResume() {
        super.onResume();
        loadData= new LoadData();
        loadData.execute(AnimeAplication.detail);

    }

    class LoadData extends AsyncTask<Integer, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid) {



        }

        @Override
        protected Void doInBackground(Integer... params) {
//            String data= Json.getJSONString(Constants.postUrl);

            String data= Json.getJSONString("http://anime-pictures.net/pictures/view_post/"+params[0].intValue()+"?type=json&lang=en");
            Call<AnimeDetail> call= AnimeAplication.service.getDetail("http://192.168.137.1/anime/post");
            call.enqueue(new Callback<AnimeDetail>() {
                @Override
                public void onResponse(Call<AnimeDetail> call, Response<AnimeDetail> response) {
                    if(response.isSuccessful()){
                        result=response.body();
                    }
                }

                @Override
                public void onFailure(Call<AnimeDetail> call, Throwable t) {

                }
            });
            return null;
        }
    }
}
