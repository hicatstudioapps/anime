package com.hicat.animepictures.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hicat.animepictures.R;
import com.hicat.animepictures.AnimeAplication;
import com.hicat.animepictures.PictureActivity;
import com.hicat.animepictures.model.Post;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 02/03/2016.
 */
public class PageRV extends RecyclerView.Adapter<PageRV.ViewHolder> {
    int start=0;
    Context context;
    List<Post> list;
    public PageRV(Context context, List<Post> list) {
        this.list=list;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
//            if(position%2==0)
//                holder.root.setBackgroundColor(Color.BLUE);
            //String imgUrl= AnimeAplication.post.getJSONObject(start+position).getString("medium_preview");
            Post data=list.get(position);
            holder.rate.setText(data.getScore().intValue()+"");
            ImageLoader.getInstance().displayImage(Uri.parse(data.getMediumPreview()).toString(), holder.image);
            int bgColor= Color.rgb(data.getColor().get(0),data.getColor().get(1), data.getColor().get(2));
            holder.root.setBackgroundColor(bgColor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @Bind(R.id.imageView2)
        ImageView image;
        @Bind(R.id.rate)
        TextView rate;
        View root;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            rate= (TextView)itemView.findViewById(R.id.rate);
            image=(ImageView)itemView.findViewById(R.id.imageView2);
            root=itemView;
        }

        @Override
        public void onClick(View v) {

//                Log.d("data",list.getJSONObject(getAdapterPosition()).toString());
                AnimeAplication.detail=list.get(getAdapterPosition()).getId();
                AnimeAplication.big_preview=list.get(getAdapterPosition()).getBigPreview();
                AnimeAplication.medium_preview=list.get(getAdapterPosition()).getMediumPreview();
                AnimeAplication.width=list.get(getAdapterPosition()).getWidth()+"";
                AnimeAplication.height=list.get(getAdapterPosition()).getHeight()+"";
                AnimeAplication.ext=list.get(getAdapterPosition()).getExt();
                AnimeAplication.score_number=list.get(getAdapterPosition()).getScoreNumber()+"";
                AnimeAplication.color=list.get(getAdapterPosition()).getColor();
                AnimeAplication.size=list.get(getAdapterPosition()).getSize()+"";
                context.startActivity(new Intent(context, PictureActivity.class));

        }
    }
}
